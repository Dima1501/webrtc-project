import Vue from 'vue'
import Vuex from 'vuex'

import firebase from 'firebase/app'
require('firebase/auth')
require('firebase/database')
import 'firebase/firestore'

Vue.use(Vuex)

const store = () => new Vuex.Store({

  state: {

    user: {
      auth: false,
      userData: [],
      conversations: []
    },

    view: {
      loginFormOpened: true,
      loginFormType: 'registration',
      loadStart: 1000
    },  

    users: [],
    lastUserId: -1000000000000000,

    lk: {
      newLearnLangs: [],
      newNativeLangs: []
    },

    conversations: [],
    messages: {
      checkedChat: [],
      checkedChatId: ''
    },

    call: {
      incoming: false,
      video: false
    }
    
  },
  actions: {
    
    logOut: function (state) {
			firebase.auth().signOut()
				.then(function() {
          state.state.user.auth = false,
          state.state.users = [],
          state.state.lastUserId = -1000000000000000
			}).catch(function(error) {
        console.log('An error happened' + error)
			});
    },
  },

  mutations: {

    loadUsers: function (state) {
      
      var userLearnLangs = state.user.userData.learnLangs
      var userNativeLangs = state.user.userData.nativeLangs

      firebase.database().ref('users')
        .orderByChild('timestamp')
        .startAt(state.lastUserId + 1)
        .limitToFirst(state.view.loadStart)
        .once('value')
        .then(function(snapshot) {
          snapshot.forEach(function(child) {

            let cardLearnLangs = child.val().learnLangs
            let cardNativeLangs = child.val().nativeLangs

            const foundLearnLangs = cardLearnLangs.some(r=> userNativeLangs.indexOf(r) >= 0)
            const foundNativeLangs = cardNativeLangs.some(r=> userLearnLangs.indexOf(r) >= 0)

            if (foundLearnLangs  && foundNativeLangs) {
              state.users.push(child.val());
              state.lastUserId = child.val().timestamp
            }
          }.bind(this));
      }).catch((err) => {
        console.log(err)
      });
    },

    loadConversations: function (state) {

      let roomsArr = []
      let conversationArr = []
      let userId = state.user.userData.userId
      
      firebase.database().ref('usersConversations/' + userId).once('value').then((snapshot) => {
        if (snapshot.val() !== null) {
          conversationArr = snapshot.val().conversations
          state.user.conversations = snapshot.val().conversations
        } else {
          return false
        }
      }).then(() => {
        for (let i = 0; i < conversationArr.length; i++) {
          if (userId < conversationArr[i]) {
            roomsArr.push(conversationArr[i] + userId)
          } else {
            roomsArr.push(userId + conversationArr[i])
          }
        }
      }).then(() => {
        state.conversations = []
        for (let i = 0; i < roomsArr.length; i++) {
          firebase.database().ref('rooms/' + roomsArr[i]).on("value", function(snapshot) {
            state.conversations = []
            state.conversations.push(snapshot.val())
          })
        }

        firebase.database().ref('usersConversations/' + state.user.userData.userId).on('value', function (snapshot) {
          console.log('new conversation')
        })
      })
    },

    call: function (state, roomId) {

      require('webrtc-adapter')

      let pc = new RTCPeerConnection(configuration)

      var configuration = {
        iceServers: [
            {urls: "stun:23.21.150.121"},
            {urls: "stun:stun.l.google.com:19302"},
            {urls: "turn:numb.viagenie.ca", credential: "webrtcdemo", username: "louis%40mozilla.com"}
        ]
      }

      let constraints = { 
        video: true,
        audio: false
      }

      let localVideo = document.getElementById('localVideo')
      let remoteVideo = document.getElementById('remoteVideo')

      let localCandidates = []

      pc.addEventListener('icecandidate', (event) => {
        if (event.candidate) {
          localCandidates.push(event.candidate.toJSON())
        } else {
          firebase.database().ref('rooms/' + roomId.roomId + '/videoCall/').update({
            callerCandidates: localCandidates
          }).catch(err => console.log(err))
        }
      })

      pc.ontrack = (e) => {
        remoteVideo.srcObject = e.streams[0];
        remoteVideo.play()
      }

      pc.onconnectionstatechange = function(event) {
        switch(pc.connectionState) {
          case "connected":
            console.log('connected')
            break;
          case "disconnected":
            console.log('disconnected')
            break;
          case "failed":
              console.log('failed')
            break;
          case "closed":
            console.log('closed')
            break;
        }
      }

      navigator.mediaDevices.getUserMedia(constraints)
        .then((stream) => {
          localVideo.srcObject = stream
          localVideo.play()

          let tracks = stream.getTracks()

          for (const track of tracks) {
            pc.addTrack(track, stream);
          }

        })
        .then(() => {
          handleNegotiationNeededEvent()
        })
        .catch(err => console.log(err))

      let handleNegotiationNeededEvent = () => {
        
        pc.createOffer()
          .then((offer) => {
            pc.setLocalDescription(offer)
          })
          .then(() => {
            firebase.database().ref('rooms/' + roomId.roomId + '/videoCall/').update({
              firstOffer: pc.localDescription
            }).then(() => {
              let waitAnswer = true
              firebase.database().ref('rooms/' + roomId.roomId + '/videoCall/').on('value', (snapshot) => {
                if (waitAnswer && snapshot.val().firstAnswer != undefined) {
                  waitAnswer = false
                  pc.setRemoteDescription(snapshot.val().firstAnswer)
                }

                if (snapshot.val().calleeCandidates != undefined) {
                  for (let candidate of snapshot.val().calleeCandidates) {
                    pc.addIceCandidate(candidate)
                      .catch(err => console.log(err))
                  }
                }
              })
            })
          })
      }
    },

    applyCall: function (state, roomId) {

      require('webrtc-adapter')

      let configuration = {
        iceServers: [
            {urls: "stun:23.21.150.121"},
            {urls: "stun:stun.l.google.com:19302"},
            {urls: "turn:numb.viagenie.ca", credential: "webrtcdemo", username: "louis%40mozilla.com"}
        ]
      }

      let pc = new RTCPeerConnection(configuration)

      let constraints = { 
        video: true,
        audio: false
      }

      let localVideo = document.getElementById('localVideo')
      let remoteVideo = document.getElementById('remoteVideo')

      let localCandidates = []

      pc.addEventListener('icecandidate', (event) => {
        if (event.candidate) {
          localCandidates.push(event.candidate.toJSON())
        } else {
          firebase.database().ref('rooms/' + roomId.roomId + '/videoCall/').update({
            calleeCandidates: localCandidates
          }).catch(err => console.log(err))
        }
      })

      pc.ontrack = (e) => {
        remoteVideo.srcObject = e.streams[0];
        remoteVideo.play()
      }

      setInterval(() => {
        remoteVideo.play()
      }, 1000);

      firebase.database().ref('rooms/' + roomId.roomId+ '/videoCall/').once('value', (snapshot) => {
        pc.setRemoteDescription(snapshot.val().firstOffer)
          .then(() => {
            navigator.mediaDevices.getUserMedia(constraints)
            .then((stream) => {
              localVideo.srcObject = stream
              localVideo.play()
    
              let tracks = stream.getTracks()
    
              for (const track of tracks) {
                pc.addTrack(track, stream);
              }

            })
            .then(() => {
              pc.createAnswer()
                .then((answer) => {

                  pc.setLocalDescription(answer)
                    .then(() => {
                      firebase.database().ref('rooms/' + roomId.roomId + '/videoCall/').update({
                        firstAnswer: answer,
                        talk: true
                      })

                      firebase.database().ref('rooms/' + roomId.roomId + '/videoCall/').on('value', snapshot => {
                        if (snapshot.val().callerCandidates != undefined) {
                          for (let candidate of snapshot.val().callerCandidates) {
                            pc.addIceCandidate(candidate)
                              .catch(err => console.log(err))
                          }
                        } 
                      })
                    })
                    .catch(err => console.log(err))
                })
                .catch(err => console.log(err))
            })
            .catch(err => console.log(err))
          })
      })
    }
  }
})

export default store
