import Vue from 'vue'
import moment from 'moment'

Vue.filter('formatDate', function(value) {
    if (value) {
      return moment(Number(value)).format('MM/DD/YYYY hh:mm')
    }
})