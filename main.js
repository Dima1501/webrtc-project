import Vue from 'vue'
import VueFire from 'vuefire'
import firebase from 'firebase/app'
import 'firebase/firestore'

Vue.use(VueFire)
if (!firebase.apps.length) {
  firebase.initializeApp({
    apiKey: "AIzaSyDSIh0s-KsYq7SweL-xvXxhrVOSsPnEMZo",
    authDomain: "langitproduction-19a49.firebaseapp.com",
    databaseURL: "https://langitproduction-19a49.firebaseio.com",
    projectId: "langitproduction-19a49",
    storageBucket: "langitproduction-19a49.appspot.com",
    messagingSenderId: "250130034580"
  })
}

export const db = firebase.firestore()
